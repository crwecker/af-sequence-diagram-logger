﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="12008004">
	<Property Name="NI.Lib.Description" Type="Str">Copyright (c) 2013, Maxtec
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="Apply Column Widths Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Apply Column Widths Msg/Apply Column Widths Msg.lvclass"/>
		<Item Name="Export to PlantUML Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Export to PlantUML Msg/Export to PlantUML Msg.lvclass"/>
		<Item Name="Open Clicked VI Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Open Clicked VI Msg/Open Clicked VI Msg.lvclass"/>
		<Item Name="Open Project File FP Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Open Project File FP Msg/Open Project File FP Msg.lvclass"/>
		<Item Name="Set Event Color Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Set Event Color Msg/Set Event Color Msg.lvclass"/>
		<Item Name="Toggle Column Hiding Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Toggle Column Hiding Msg/Toggle Column Hiding Msg.lvclass"/>
		<Item Name="Update Call Chain Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Update Call Chain Msg/Update Call Chain Msg.lvclass"/>
		<Item Name="Update Colors using Dialog Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Update Colors using Dialog Msg/Update Colors using Dialog Msg.lvclass"/>
		<Item Name="Update Msgs to Ignore Dialog Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Update Msgs to Ignore Dialog Msg/Update Msgs to Ignore Dialog Msg.lvclass"/>
		<Item Name="Update Msgs to Ignore Msg.lvclass" Type="LVClass" URL="../../AF Sequence Diagram Logger Messages/Update Msgs to Ignore Msg/Update Msgs to Ignore Msg.lvclass"/>
	</Item>
	<Item Name="AF Sequence Diagram Logger.lvclass" Type="LVClass" URL="../AF Sequence Diagram Logger.lvclass"/>
</Library>
